# Global router

This is a realization of global planner for streets using ROS, python, osmnx

## Table Of Contents
1. [Nodes overview](#Nodes overview)  
2. [Installation](#Installation) 
3. [Docker building](#Docker building) 
4. [Docker running](#Docker running) 
5. [Building using ROS](#Building using ROS) 
6. [User Guide](#User Guide) 
7. [Requirements](#Requirements)

## Nodes overview
- global_router.py is main node, it consists class MyPlanner
- math_lib.py is math library with helpful functions

## Installation

After cloning repo you can use two options:
- Docker
- Preinstalled ROS + installing dependencies 

## Docker building

For this you should have your workspace with this repo, rviz_satelite and odom_tf_broadcaster. Go to the global_router/docker:

```
cd global_router/docker
./build.sh
```
 ## Docker running

 For running docker use ./start, there is flag -v which copy requirement packages to container:
 ```
cd global_router/docker
./start.sh
```
For starting project run:
 ```
cd global_router/docker
./into.sh
```
This script will start catkin_make in docker and after compiling automatically run the project

## Building using ROS
Firstly,  you need to clone this packages: rviz_satelite and odom_tf_broadcaster

Next step is compiling:
 ```
catkin build
```
For launching test demo script:
 ```
roslaunch global_router test_router.launch
```
## User Guide
This package use downloaded graph file for searching path, graph file is located in config/test_1.osm, path to this file was written in launch file

For configuration there is config file, config/global_router.yaml
- download_graph: False - Download new graph
- graph_area: 50 - area for downloading graph
- default_orientation_in_path: [0, 0, 0, 1] - orientation is path segments
- goal_topic: "/move_base_simple/goal" - topic for getting goal
- pub_path_topic: "/gis_rtk/waypoints" - path for output information, type is rtp_msgs/RouteTask
- show_rviz_path: True - publish path in Rviz
- show_markers: True - show markers in path nodes
- use_tf_for_start_point: True - get start point using tf from base_link to map
- use_rviz_goal: True - use Rviz nav_goal for getting goal point
- global_frame_param: "global_frame" - name of param
- robot_frame_param: "robot_frame" - name of param

You can use rviz for setting goals, for start point you alse can use rviz or use tf translation, and start point will be position of robot

## Requirements
global_frame - param, default map
robot_frame - param, default base_link
utm_zone - param, default 37U
