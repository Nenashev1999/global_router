#!/usr/bin/env python

import rospy
import tf2_ros
from rtp_msgs.msg import PathPointWithMetadata, PathPointMetadata, RouteTask
from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseStamped, Pose, Quaternion
from nav_msgs.msg import Path

import utm
import osmnx as ox

import random
import warnings

from math_lib import *

warnings.filterwarnings("ignore")


class MyPlanner(object):
    def __init__(self):
        self.global_frame = rospy.get_param("/global_router/global_frame", "map")
        self.robot_frame = rospy.get_param("/global_router/robot_frame", "base_link")
        zone = rospy.get_param("/coordinate_transformer/utm_zone", "37north")
        self.area = rospy.get_param("/global_router/graph_area", "1000")
        self.graph_path = rospy.get_param("/global_router/graph_path", "/config/test_1.osm")
        self.utm_number_zone = int(zone[:2])
        self.utm_zone_id = zone[2:]
        print(self.utm_number_zone, self.utm_zone_id)

        self.use_tf_for_start_point = rospy.get_param("/global_router/use_tf_for_start_point", True)
        self.show_rviz_path = rospy.get_param("/global_router/show_rviz_path", True)
        self.show_markers = rospy.get_param("/global_router/show_markers", True)
        self.use_rviz_goal = rospy.get_param("/global_router/use_rviz_goal", True)
        goal_topic = rospy.get_param("/global_router/goal_topic", "/move_base_simple/goal")
        pub_path_topic = rospy.get_param("/global_router/pub_path_topic", "/gis_rtk/waypoints")
        if not self.use_tf_for_start_point:
            self.start_point_sub = rospy.Subscriber("/initialpose", PoseWithCovarianceStamped, self.start_point_callback)
        if self.use_rviz_goal:
            self.goal_point_rviz_sub = rospy.Subscriber(goal_topic, PoseStamped, self.goal_point_callback)
            # self.goal_point_robot_topic_sub = rospy.Subscriber(goal_topic, PoseStamped,
            #                                                self.goal_point_robot_topic_callback)
        if self.show_markers:
            self.edge_pub_1 = rospy.Publisher("/global_router/marker_path_nodes", MarkerArray, queue_size=1)
            self.edge_pub_2 = rospy.Publisher("/global_router/marker_crossing", MarkerArray, queue_size=1)
        if self.show_rviz_path:
            self.path_pub = rospy.Publisher("/global_router/path", Path, queue_size=1)
        self.path_pub_task = rospy.Publisher(pub_path_topic, RouteTask, queue_size=1)

        self.start_point_lat = None
        self.goal_point_lat = None
        self.start_point_utm = None
        self.goal_point_utm = None
        self.graph = None
        self.path = None
        self.path_points_utm = None
        self.path_points_lla = None
        self.path_length = None
        self.start_point_flag = False

        orientation = rospy.get_param("/global_router/default_orientation_in_path", [0, 0, 0, 1])
        self.default_orientation = Quaternion(orientation[0], orientation[1], 
                                              orientation[2], orientation[3])
        self.tfBuffer = tf2_ros.Buffer()
        self.tfListener = tf2_ros.TransformListener(self.tfBuffer)

        download_graph = rospy.get_param("/global_router/download_graph", False)
        if download_graph:
            while self.graph is None:
                try:
                    trans = self.tfBuffer.lookup_transform(self.global_frame, self.robot_frame, rospy.Time(0))
                    point = utm.to_latlon(trans.transform.translation.x, trans.transform.translation.y,
                                          self.utm_number_zone, self.utm_zone_id)
                    rospy.loginfo("Start downloading graph %s", point)
                    start_download_time = rospy.Time.to_sec()
                    self.graph = ox.graph_from_point(point, dist=self.area, network_type="walk")
                    rospy.loginfo("Graph successfully downloaded, it took %s secs",
                                  rospy.Time.to_sec() - start_download_time)
                except:
                    rospy.loginfo("Waiting for transform")
                    rospy.sleep(1.0)
        else:
            self.graph = ox.load_graphml(self.graph_path)
            rospy.loginfo("Graph successfully initialized")

    def start_point_callback(self, msg):
        try:
            self.start_point_utm = msg.pose.pose
            self.start_point_lat = utm.to_latlon(msg.pose.pose.position.x, msg.pose.pose.position.y,
                                                self.utm_number_zone, self.utm_zone_id)
            rospy.loginfo("Start: LLA: %s UTM: %s", self.start_point_lat,
                        (round(msg.pose.pose.position.x, 3), round(msg.pose.pose.position.y, 3),
                        self.utm_number_zone, self.utm_zone_id))
            self.start_point_flag = True
        except:
            rospy.logwarn("Invalid point")

    def get_start_point_from_transform(self):
        trans = self.tfBuffer.lookup_transform(self.global_frame, self.robot_frame, rospy.Time(0))
        coord = Pose()
        coord.position = trans.transform.translation
        coord.orientation = trans.transform.rotation
        self.start_point_utm = coord
        self.start_point_lat = utm.to_latlon(trans.transform.translation.x, trans.transform.translation.y,
                                             self.utm_number_zone, self.utm_zone_id)
        rospy.loginfo("Start: LLA: %s UTM: %s", self.start_point_lat,
                      (round(trans.transform.translation.x, 3), round(trans.transform.translation.y, 3),
                       self.utm_number_zone, self.utm_zone_id))
        self.start_point_flag = True

    def goal_point_robot_topic_callback(self, msg):
        pass

    def goal_point_callback(self, msg):
        try:
            if self.use_tf_for_start_point:
                self.get_start_point_from_transform()
            self.goal_point_utm = msg.pose
            self.goal_point_lat = utm.to_latlon(msg.pose.position.x, msg.pose.position.y,
                                                self.utm_number_zone, self.utm_zone_id)
            rospy.loginfo("Goal: LLA: %s UTM: %s", self.goal_point_lat,
                        (round(msg.pose.position.x, 3), round(msg.pose.position.y, 3),
                        self.utm_number_zone, self.utm_zone_id))
            if self.start_point_flag:
                self.calculate_path()
            else:
                rospy.logwarn("Please, set origin point")
        except:
            rospy.logwarn("Invalid point")

    def calculate_path(self):
        try:
            start_time = rospy.get_time()   
            start_node = self.get_adapted_node(self.start_point_lat, self.start_point_utm)
            goal_node = self.get_adapted_node(self.goal_point_lat, self.goal_point_utm)
            route = ox.shortest_path(self.graph, start_node, goal_node, weight='length')
            self.get_path_points_from_nodes(route)
            self.get_path_length()
            rospy.loginfo("Route successfully created, length: %s m, it took: %s secs",
                        self.path_length, round(rospy.get_time() - start_time, 3))
            rospy.loginfo("-----------------------------------------------------------------------------------------")
            if self.show_rviz_path:
                self.show_path_rviz()

            self.check_route_crossing(route)
            self.pub_path_task()
            self.start_point_flag = False
        except:
            rospy.logwarn("Errors in creating path")

    def get_adapted_node(self, lat, utm_coords):
        nearest_nodes = ox.distance.nearest_edges(self.graph, X=lat[1], Y=lat[0])
        if "geometry" in self.graph.edges.get((nearest_nodes[0], nearest_nodes[1], 0)):
            self.split_complicated_geometry(nearest_nodes)
            nearest_nodes = ox.distance.nearest_edges(self.graph, X=lat[1], Y=lat[0])

        first_node = utm.from_latlon(self.graph.nodes[nearest_nodes[0]]["y"], self.graph.nodes[nearest_nodes[0]]["x"])
        second_node = utm.from_latlon(self.graph.nodes[nearest_nodes[1]]["y"], self.graph.nodes[nearest_nodes[1]]["x"])
        projection_point = get_projection_of_point_to_line(first_node, second_node, (utm_coords.position.x, utm_coords.position.y))
        if is_point_in_edge(first_node, second_node, projection_point):
            new_node_id = self.add_nodes_and_edges(first_node, projection_point, nearest_nodes[0])
            new_node_id = self.add_nodes_and_edges(second_node, projection_point, nearest_nodes[1], new_node_id)
            return new_node_id
        else:
            if euclidean_distance(first_node, (utm_coords.position.x, utm_coords.position.y)) < \
                    euclidean_distance(second_node, (utm_coords.position.x, utm_coords.position.y)):
                return nearest_nodes[0]
            else:
                return nearest_nodes[1]
            
    def split_complicated_geometry(self, nearest_nodes):
        parts = self.graph.edges.get((nearest_nodes[0], nearest_nodes[1], 0))['geometry'].coords.xy
        prev_node = nearest_nodes[0]
        for j in range(len(parts[0]) - 2):
            first_coord_node = utm.from_latlon(parts[1][j], parts[0][j])
            second_coord_node = utm.from_latlon(parts[1][j + 1], parts[0][j + 1])
            prev_node = self.add_nodes_and_edges(first_coord_node, second_coord_node, prev_node)
            
        first_coord_node = utm.from_latlon(parts[1][-2], parts[0][-2])
        second_coord_node = utm.from_latlon(parts[1][-1], parts[0][-1])
        prev_node = self.add_nodes_and_edges(first_coord_node, second_coord_node, nearest_nodes[1], prev_node)
        self.graph.remove_edges_from([(nearest_nodes[0], nearest_nodes[1]), 
                                 (nearest_nodes[1], nearest_nodes[0])])

    def add_nodes_and_edges(self, existing_node_coords, new_node_coords, node_1, node_2=None):
        if node_2 is None:
            node_id = random.randint(0, 1000)
            while self.graph.has_node(node_id):
                node_id = random.randint(0, 1000)
        else:
            node_id = node_2
        new_coords_lla = utm.to_latlon(new_node_coords[0], new_node_coords[1], self.utm_number_zone,
                                            self.utm_zone_id)
        if node_2 is None:
            self.graph.add_nodes_from([(node_id, {"y": new_coords_lla[0], "x": new_coords_lla[1]})])
        self.graph.add_edges_from([(node_id, node_1,
                                    {'length': euclidean_distance(existing_node_coords, new_node_coords)}),
                                    (node_1, node_id,
                                    {'length': euclidean_distance(existing_node_coords, new_node_coords)})])
        return node_id

    def get_path_points_from_nodes(self, route):
        path_utm = [(self.start_point_utm.position.x, self.start_point_utm.position.y)]
        path_lla = [(self.start_point_lat[0], self.start_point_lat[1])]
        for i in range(len(route)):
            if i < (len(route) - 1) and "geometry" in self.graph.edges.get((route[i], route[i + 1], 0)):
                parts = self.graph.edges.get((route[i], route[i + 1], 0))['geometry'].coords.xy
                for j in range(len(parts[0])):
                    path_lla.append((parts[1][j], parts[0][j]))
                    path_utm.append(utm.from_latlon(parts[1][j], parts[0][j]))
            else:
                path_utm.append(utm.from_latlon(self.graph.nodes[route[i]]["y"], self.graph.nodes[route[i]]["x"]))
                path_lla.append((self.graph.nodes[route[i]]["y"], self.graph.nodes[route[i]]["x"]))
        path_utm.append((self.goal_point_utm.position.x, self.goal_point_utm.position.y))
        path_lla.append((self.goal_point_lat[0], self.goal_point_lat[1]))
        self.path_points_utm = path_utm
        self.path_points_lla = path_lla

    def show_path_rviz(self):
        if self.show_markers:
            nodes_k = []
        self.path = Path()
        self.path.header.frame_id = "map"
        self.path.header.stamp = rospy.Time.now()
        for i in range(len(self.path_points_utm)):
            self.add_segment_to_path(self.path_points_utm[i])
            if self.show_markers:
                nodes_k.append(self.path_points_utm[i])
        self.path_pub.publish(self.path)
        if self.show_markers:
            self.display_edge_1(nodes_k)
    
    def pub_path_task(self):
        path = RouteTask()
        path.header.frame_id = "map"
        path.header.stamp = rospy.Time.now()
        segment = self.add_segment_path_task((self.path_points_lla[0][1], self.path_points_lla[0][0]), self.start_point_utm.orientation)
        path.path_with_metadata.append(segment)
        for i in range(1, len(self.path_points_lla) - 1): 
            segment = self.add_segment_path_task(self.path_points_lla[i], self.default_orientation)
            path.path_with_metadata.append(segment)
        segment = self.add_segment_path_task((self.path_points_lla[-1][1], self.path_points_lla[-1][0]), self.goal_point_utm.orientation)
        path.path_with_metadata.append(segment)
        self.path_pub_task.publish(path)
    
    def add_segment_path_task(self, coords, orientation):
        point = PathPointWithMetadata()
        point.pose.position.x = coords[1]
        point.pose.position.y = coords[0]
        point.pose.orientation = orientation
        point.metadata.linear_velocity = 1.0
        point.metadata.key_point = True
        return point

    def get_path_length(self):
        cost = 0
        for i in range(len(self.path_points_utm) - 1):
            cost += euclidean_distance(self.path_points_utm[i], self.path_points_utm[i + 1])
        self.path_length = cost

    def check_route_crossing(self, route):
        crossing = []
        for i in range(len(route)):
            if "highway" in self.graph.nodes.get(route[i]):
                if self.graph.nodes.get(route[i])["highway"] == "crossing":
                    crossing.append(utm.from_latlon(self.graph.nodes[route[i]]["y"], self.graph.nodes[route[i]]["x"]))
        if len(crossing) > 0:
            self.display_crossing(crossing)

    def get_path_cost(self, route):
        cost = 0.0
        for i in range(len(route) - 1):
            cost += self.graph.edges.get((route[i], route[i + 1], 0))['length']
        return cost

    def add_segment_to_path(self, coords_utm):
        pose = PoseStamped()
        pose.header.frame_id = "map"
        pose.pose.position.x = coords_utm[0]
        pose.pose.position.y = coords_utm[1]
        self.path.poses.append(pose)

    def display_edge_1(self, edges):
        marker = []
        for i, position in enumerate(edges):
            point = Marker()
            point.header.stamp = rospy.Time.now()
            point.header.frame_id = "map"
            point.id = i
            point.type = 3
            point.ns = "2"
            point.pose.position.x = position[0]
            point.pose.position.y = position[1]
            point.pose.position.z = 0
            point.pose.orientation.w = 1
            point.scale.x = 2.1
            point.scale.y = 2.1
            point.scale.z = 1.1
            point.color.a = 1
            point.color.r = 1
            marker.append(point)
        self.edge_pub_1.publish(marker)

    def display_crossing(self, edges):
        marker = []
        for i, position in enumerate(edges):
            point = Marker()
            point.header.stamp = rospy.Time.now()
            point.header.frame_id = "map"
            point.id = i
            point.type = 3
            point.ns = "1"
            point.pose.position.x = position[0]
            point.pose.position.y = position[1]
            point.pose.position.z = 0
            point.pose.orientation.w = 1
            point.scale.x = 10.1
            point.scale.y = 10.1
            point.scale.z = 1.1
            point.color.a = 1
            point.color.b = 1
            marker.append(point)
        self.edge_pub_2.publish(marker)


def main():
    rospy.init_node("test_planner_node")
    planner = MyPlanner()
    rospy.spin()


if __name__ == "__main__":
    main()
