def get_projection_of_point_to_line(a1, a2, a3):
    b1 = a3[0] * (a1[0] - a2[0]) + a3[1] * (a1[1] - a2[1])
    b2 = a1[0] * a2[1] - a1[1] * a2[0]

    y = (a1[0] - a2[0]) * (a1[0] - a2[0]) + (a1[1] - a2[1]) * (a1[1] - a2[1])
    det_k = b1 * (a1[0] - a2[0]) - b2 * (a1[1] - a2[1])

    x = det_k / y
    det_k = (a1[0] - a2[0]) * b2 + (a1[1] - a2[1]) * b1
    y = det_k/y
    return x, y


def euclidean_distance(a1, a2):
    return ((a1[0] - a2[0]) ** 2 + (a1[1] - a2[1]) ** 2) ** 0.5


def is_point_in_edge(a1, a2, a3):
    if min(a1[0], a2[0]) <= a3[0] <= max(a1[0], a2[0]) or min(a1[1], a2[1]) <= a3[1] <= max(a1[1], a2[1]):
        return True
    return False