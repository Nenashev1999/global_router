#!/bin/bash
docker exec --user "docker_tf" -it global_router \
        /bin/bash -c "source /ros_entrypoint.sh && cd /home/docker_tf/catkin_ws && catkin_make && 
        source ~/catkin_ws/devel/setup.bash && roslaunch global_router test_router.launch && /bin/bash"
