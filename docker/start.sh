#!/bin/bash

orange=`tput setaf 3`
reset_color=`tput sgr0`

export ARCH=`uname -m`

echo "Running on ${orange}${ARCH}${reset_color}"

cd "$(dirname "$0")"
cd ..
workspace_dir=$PWD

if [ "$(docker ps -aq -f name=global_router)" ]; then
    docker stop global_router;
fi

docker run -it -d --rm \
        --env="DISPLAY=$DISPLAY" \
        --env="QT_X11_NO_MITSHM=1" \
        --privileged \
        --name global_router \
        --net "host" \
        -v /tmp/.X11-unix:/tmp/.X11-unix:rw \
        -v $workspace_dir/:/home/docker_tf/catkin_ws/src:rw \
        ${ARCH}noetic/global_router:latest 
        
