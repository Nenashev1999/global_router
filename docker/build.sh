#!/bin/bash

orange=`tput setaf 3`
reset_color=`tput sgr0`

cd "$(dirname "$0")"
root_dir=$PWD 
cd $root_dir

ARCH=`uname -m`

echo "Building for ${orange}${ARCH}${reset_color}"

docker build . \
    -f $root_dir/Dockerfile \
    --build-arg UID=$(id -g) \
    --build-arg GID=$(id -g) \
    -t ${ARCH}noetic/global_router:latest
